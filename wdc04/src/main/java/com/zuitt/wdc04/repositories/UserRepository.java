package com.zuitt.wdc04.repositories;

import com.zuitt.wdc04.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface UserRepository extends CrudRepository<User, Object> {
    User findByUsername (String username);
}
