package com.zuitt.wdc04.models;

import javax.persistence.*;

//mark this Java object as a representation of a database table via @Entity

@Entity
//designate table via @Table
@Table(name="posts")
public class Post {
    @Id

    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private  String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;



    // Default constructor
    public Post (){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
