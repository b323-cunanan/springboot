package com.zuitt.wdc04.controllers;

import com.zuitt.wdc04.models.Post;
import com.zuitt.wdc04.services.PostService;
import com.zuitt.wdc04.services.PostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
//Enable cross origin requests via @CrossOrigin
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        return postService.updatePost(postid, stringToken, post);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(postid, stringToken);
    }



    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String stringToken) {
        return postService.getUserPosts(stringToken);
    }
    //Retrieve all post
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public Iterable<Post> getPosts(){
        return postService.getPosts();
    }
}