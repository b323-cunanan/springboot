package com.zuitt.wdc04.services;

import com.zuitt.wdc04.config.JwtToken;
import com.zuitt.wdc04.models.Post;
import com.zuitt.wdc04.models.User;
import com.zuitt.wdc04.repositories.PostRepository;
import com.zuitt.wdc04.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.ArrayList;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(Long id, String stringToken){
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity getUserPosts(String stringToken){

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User user = userRepository.findByUsername(authenticatedUser);
        if (user != null) {
            long userId = user.getId();
            List<Post> allPosts = (List<Post>) postRepository.findAll();

            List<Post> userPosts = new ArrayList<>();

            for (Post post : allPosts) {
                if (post.getUser().getId() == userId) {
                    userPosts.add(post);
                }
            }

            return ResponseEntity.ok(userPosts);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }

    }


    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

}
