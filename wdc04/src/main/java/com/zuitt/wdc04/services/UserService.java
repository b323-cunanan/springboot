package com.zuitt.wdc04.services;

import com.zuitt.wdc04.models.User;

import java.util.Optional;

public interface UserService {

    void createUser(User user);

    Optional<User> findByUsername(String username);
}
