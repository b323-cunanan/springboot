package com.zuitt.wdc04.exceptions;

public class UserException extends Exception {
    public UserException(String message){ super(message);}
}
